#include <stdio.h>

#ifdef KLEE
#include <klee/klee.h>
#endif

int linear_find_in_array(int n, int t[], int x) {
  int i = 0;
  while(t[i] < x && i < n) i++;
  if (t[i] == x) return i;
  return -1;
}

#define N 10

int main() {
  int t[N];
#ifdef KLEE
  klee_make_symbolic(t, sizeof(t), "t");
#else
  for(int i=0; i<N; i++) t[i] = i;
#endif
  printf("%d\n", linear_find_in_array(N, t, 7));
  return 0;
}
