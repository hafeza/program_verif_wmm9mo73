#include <stdio.h>
#include <klee/klee.h>

int main() {
  int x;
  klee_make_symbolic(&x, sizeof(x), "x");
  if (x < 0) {
    printf("negative\n");
  }
  if (x > 1000) {
    printf("large\n");
  }
  return 0;
}
