#include <stdio.h>
#include <assert.h>
#include <klee/klee.h>

#define N 10
#define MAX 20

int main() {
  int t[N], sum = 0;
  char name[20];
  for(int i=0; i<N; i++) {
    snprintf(name, sizeof(name), "t[%d]", i);
    t[i] = klee_range(0, MAX, name);
    sum += t[i];
  }
  klee_assert(sum < MAX * N);
  return 0;
}
