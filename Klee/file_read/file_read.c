#include <stdio.h>
#include <stdlib.h>

int main() {
  FILE *fp = stdin;
  while(! feof(fp)) {
    int dat;
    if (fscanf(fp, "%d\n", &dat) != 1) {
      exit(1);
    }
    char out[13];
    sprintf(out, "data = %d", dat);
    puts(out);
  }
  return 0;
}
