#include <stdio.h>
#include <klee/klee.h>

int main() {
  klee_make_symbolic(&dat, sizeof(dat), "dat");
  char out[13];
  sprintf(out, "data = %d", dat);
  return 0;
}
