#include <stdio.h>
#include <stdlib.h>

#ifdef KLEE
#include <klee/klee.h>
int read_int(FILE *fp, int *dat, const char *id) {
  if (klee_range(0, 2, "read_int_available")) {
    klee_make_symbolic(dat, sizeof(*dat), id);
    return 1;
  } else {
    return 0;
  }
}
#else
int read_int(FILE *fp, int *dat, const char *id) {
  return fscanf(fp, "%d\n", dat);
}
#endif

int main() {
  FILE *fp = stdin;
  while(! feof(fp)) {
    int dat;
    if (read_int(fp, &dat, "dat") != 1) exit(1);
    char out[13];
    sprintf(out, "data = %d", dat);
#ifndef KLEE
    puts(out);
#endif
  }
  return 0;
}
