# Program testing and verification (WMM9MO73)

by David Monniaux (DM), Sylvain Boulmé (SB), Radu Iosif (RI) and Lionel Rieg (LR)

**emails** firstname.name@imag.fr

Please clone this repository and keep it synchronized when teachers update it.

## Contents

1. **27 sep** Course (DM) - test and properties
2. **04 oct** Course (DM) - undecidability
3. **11 oct** Course (DM) - concolic testing
4. **18 oct** Computer session (DM) - Klee (1/2)
5. **25 oct** Computer session (DM) - Klee (2/2)
6. **08 nov** Computer session (SB) - Tutorial to Frama-C WP plugin (1/2)
    + **Before the session**, please read *Section 1* (6 pages) of the [introduction to Frama-C WP plugin](Tutorial-Frama-C-WP/frama-C-wp-tutorial.pdf)
    + At the beginning of the session, read instructions in *Section 2* and get the [provided files](Tutorial-Frama-C-WP/PROVIDED_FILES/)
7. **15 nov** Computer session (SB) - Tutorial to Frama-C WP plugin (2/2)
    + Continue on the [provided files](Tutorial-Frama-C-WP/PROVIDED_FILES/)
8. **22 nov** Course (SB) - introduction to Hoare logic
    + [slides](intro_hoare_logic_handout.pdf) or [compact version in 4up](intro_hoare_logic_handout-4up.pdf)
9. **29 nov** Course (DM) - abstract interpretation
10. **06 dec** Course (RI) - separation logic
    + Additional reading 1: http://www0.cs.ucl.ac.uk/staff/p.ohearn/papers/bi-assertion-lan.pdf
    + Additional reading 2: https://www.cs.cmu.edu/~jcr/seplogic.pdf

11. **13 dec** Computer session (LR) (1/2)
    + **Before the session**, please [download](https://github.com/verifast/verifast/releases) and install the VeriFast tool (version 21.04) and make sure you can run the IDE (vfide).
    + For the session, you will need to download two files: the short [verifast-tutorial](tutorial-VeriFast/verifast-tutorial.pdf) and the exercice file [tp-verifast.v](tutorial-VeriFast/tp-verifast.c).
12. **3 jan** Computer session (LR) (2/2)

## Mandatory Frama-C Homework (part of WMM9MO73 ECTS)


Complete the following files of the *Frama-C Tutorial*: [`div1.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/div1.c),
[`linear_search.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/linear_search.c), [`array_copy.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/array_copy.c),
 [`min_sort.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/min_sort.c) and [`binary_search.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/binary_search.c).

### How to submit ###

Please, respect the submission format (see below).
If you have an Ensimag account, please submit your archive on [the dedicated Teide website](https://teide.ensimag.fr/interface_view_project.php?view=products&id=2983&prod=3526).
Otherwise, simply email your archive to [Sylvain Boulmé](mailto:Sylvain.Boulme@imag.fr?subject=[Frama-C Submission]).

**Deadline: Friday, December 3 at 18h00**

### Format of the submission ###

Your submission should consists of a `.tar.gz` or a `.zip` archive containing your completed `*.c`. 
Your archive may also contain a `README` or `README.md` pointing the issues or the features of your solutions.

Your submission should contain *at most* one **incomplete** solution. 
This simply means that if you already have an unfinished file, then you should not start to work on another file.

A solution is complete, iff all WP Goals are marked `Valid` by `frama-c -wp -wp-rte -wp-split` with two exceptions (i.e. the following goals are marked as `Timeout`):

  * `buggy_call_div_requires` in `div1.c` (this is a test that `frama-c` rejects incorrect aliases).
  * `typed_main_assert` in `binary_search.c` (this is due to incomplete support of `calloc` by `frama-c`).

Feel free to email [Sylvain Boulmé](mailto:Sylvain.Boulme@imag.fr?subject=[Frama-C Question]) in case of trouble with Frama-C or with this homework.
