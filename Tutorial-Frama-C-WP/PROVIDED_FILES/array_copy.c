// TODO: 
//   1. fix array_copy to make it respect the initial specification
//   2. remove the "\separated" precondition in order to make valid
//      the calls to "array_copy" in the main function.
//   3. fix array_copy to make it respect this weaker precondition.

/*@ requires \valid(dest+(0..size-1)) && \valid(srce+(0..size-1)) 
  @          && \separated(dest+(0..size-1),srce+(0..size-1))  ;
  @ assigns  dest[0..size-1];
  @ ensures \forall int i; 0 <= i < size ==> dest[i]==\old(srce[i]); 
  @*/
void array_copy(int* dest, int* srce, int size) 
{
  int i;
  if (size <= 0 || dest==srce) return;
  i=0;
  /*@ loop invariant 0 <= i <= size;
    @ loop invariant \forall int k; 0 <= k < i ==> dest[k]==\at(srce[k],Pre);
    @ loop invariant \forall int k; i <= k < size ==> srce[k]==\at(srce[k],Pre);
    @ loop assigns dest[0..size-1], i; 
    @ */
  while (i <= size) {
    dest[i]=srce[i];
    i++;
  }
}

int main() {
  int aux[] = { 10, 20, 30 };
  int tab[3] ;
  array_copy(tab,aux,3);
  //@ assert tab[0]==10 && tab[1]==20 && tab[2]==30;
  array_copy(tab,tab+1,2);
  //@ assert tab[0]==20 && tab[1]==30 && tab[2]==30;
  array_copy(tab+1,tab,2);
  //@ assert tab[0]==20 && tab[1]==20 && tab[2]==30;
  return tab[2];
}
