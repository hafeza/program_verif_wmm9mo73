// TODO: only observe that it behaves from frama-c like in the pdf. 

#include "any.h"
#define MAXINT 2147483647


/*@ requires \valid(x) && *x < MAXINT;
  @ assigns *x;
  @ ensures \result == \old(*x) && *x == \result+1;
  @*/
int incr(int* x) {
  return (*x)++;
}


int main() {
  int x = any();
  int r;
  if (x == MAXINT) return -1;
  r=incr(&x);
  /*@ assert x > r; */
  if (x <= r) {
    /*@ assert 0==1; */
    return 1/(r+1-x);
  }
  return x;
}
