// TODO: only observe that it behaves from frama-c like in the pdf. 

/*@ assigns \nothing;
  @ ensures \result >= x; */
int max20(int x) {

  /*@ loop invariant \at(x,Pre) <= x;
    @ loop assigns x; */
  while (x <= 19) {
    x++;
  }

  return x;
}
