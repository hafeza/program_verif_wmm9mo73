#define MAXINT 2147483647
#include "any.h"

/*@ requires 0 <= x;
  @ requires x*17 <= MAXINT;
  @ assigns \nothing;
  @ ensures \result == x * 17;
  @*/
int mult17(int x) {
  int z=0;

  // TODO: fix the bug in this while-loop code and prove that you have fixed it!
  /*@ loop invariant 0 <= x ;
      loop invariant \at(x,Pre)*17==z+x*17 ; 
      loop assigns z,x ; */
  while (x >= 0) {
    z+=17;
    x--;
  }
  return z;
}


int main() {
  int x=any(), r;

  if (x < 0 || x > 16) x=15; 
  
  r=mult17(x);

  /*@ assert r == x*17; */

  return r;
}
