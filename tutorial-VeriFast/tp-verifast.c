#include "stdlib.h"


struct account {
    int balance;
    char *owner;
};

int illegal_access()
//@ requires true;
//@ ensures result == 0;
{
    struct account *myAccount = malloc(sizeof(struct account));
    //if (myAccount == 0) { abort(); }
    myAccount->balance = 5;
    free(myAccount);
    return 0;
}


/*****************************************/
/*  First exercise : account management  */
/*****************************************/

// Question 1: clear_balance
/* UNCOMMENT
void clear_balance(struct account *myAccount)
//@ requires true;
//@ ensures true;
{
    myAccount->balance = 0;
}
*/

// Question 2: set_balance


// Question 3: get_balance


// Qestion 4: test_account



/******************************************/
/*  List: a first foray into predicates   */
/******************************************/

typedef struct list_node_t {
    struct list_node_t *next;
    int value;
} * list_node;

/*@
predicate mylist(list_node l1, list_node l2) =
  l1 == l2 ?
    true
  :
    l1 != 0 &*& malloc_block_list_node_t(l1) &*& l1->value |-> _
    &*& l1->next |-> ?l3 &*& mylist(l3,l2);

predicate mylist_ptr(list_node *ptr) =
  malloc_block_pointers(ptr,1) &*& ptr != 0 &*& *ptr |-> ?start &*& mylist(start, 0);
@*/

// Question 5: create_empty_list
list_node *create_empty_list()
//@ requires true;
//@ ensures mylist_ptr(result);
{
    list_node *l = malloc(sizeof(list_node));
    if (l == 0)	{ abort (); }
    *l = 0;
    //@ close mylist(*l,*l);
    //@ close mylist_ptr(l);
    return l;
}

// Question 6: list_push
/* UNCOMMENT
void list_push(list_node *l, int value)
//@ requires mylist_ptr(l);
//@ ensures mylist_ptr(l);
{
    list_node n = malloc(sizeof(struct list_node_t));
    if (n == 0) { abort(); }
    n->next = *l;
    n->value = value;
    *l = n;
}
*/

// Question 7: list_pop and list_dispose
/* UNCOMMENT
int list_pop(list_node *l)
{
    list_node head = *l;
    if (head == 0) { abort(); }
    int result = head->value;
    *l = head->next;
    free(head);
    return result;
}

void list_dispose(list_node *l)
{
    if (*l != 0) { abort (); }
    free(l);
}
*/

// Question 8: first_test
/* UNCOMMENT
int first_test()
//@ requires true;
//@ ensures true;
{
    list_node *s = create_empty_list();
    list_push(s, 10);
    list_push(s, 20);
    int result1 = list_pop(s);
    //assert(result1 == 20);
    int result2 = list_pop(s);
    //assert(result2 == 10);
    list_dispose(s);
    return 0;
}
*/

// Question 9: list_reverse
/* UNCOMMENT
void list_reverse(list_node *l)
{
    list_node n = *l;
    list_node m = 0;
    while (n != 0)
    {
        list_node next = n->next;
        n->next = m;
        m = n;
        n = next;
    }
    *l = m;
}
*/


/******************************************/
/*  List: a more precise specification    */
/******************************************/

/*@
inductive ints = ints_nil | ints_cons(int, ints);

fixpoint int ints_head(ints values) {
    switch (values) {
        case ints_nil: return 0;
        case ints_cons(value, values0): return value;
    }
}

fixpoint ints ints_tail(ints values) {
    switch (values) {
        case ints_nil: return ints_nil;
        case ints_cons(value, values0): return values0;
    }
}

predicate nodes(list_node start, list_node end, ints values) =
    start == end ?
        values == ints_nil
    :
        start->next |-> ?next &*& start->value |-> ?value &*& malloc_block_list_node_t(start) &*&
        nodes(next, end, ?values0) &*& values == ints_cons(value, values0);

predicate nodes_ptr(list_node *l, ints values) =
    *l |-> ?head &*& malloc_block_pointers(l,1) &*& nodes(head, 0, values);
@*/

// Question 10: create_empty_nodes, nodes_push, nodes_pop and nodes_dispose
list_node *create_empty_nodes()
{
    list_node *l = malloc(sizeof(list_node));
    if (l == 0)	{ abort (); }
    *l = 0;
    return l;
}

void nodes_push(list_node *l, int value)
{
    list_node n = malloc(sizeof(struct list_node_t));
    if (n == 0) { abort(); }
    n->next = *l;
    n->value = value;
    *l = n;
}

int nodes_pop(list_node *l)
{
    list_node head = *l;
    if (head == 0) { abort(); }
    int result = head->value;
    *l = head->next;
    free(head);
    return result;
}

void nodes_dispose(list_node *l)
{
    if (*l != 0) { abort (); }
    free(l);
}


// Question 12: first_test with assertions


// Question 13: list_reverse

// The logical functions ints_append and int_reverse
/*@
fixpoint ints ints_append(ints values1, ints values2) {
    switch (values1) {
        case ints_nil: return values2;
        case ints_cons(h, t): return ints_cons(h, ints_append(t, values2));
    }
}

fixpoint ints ints_reverse(ints values) {
    switch (values) {
        case ints_nil: return ints_nil;
        case ints_cons(h, t): return ints_append(ints_reverse(t), ints_cons(h, ints_nil));
    }
}
@*/

// The four lemmas
/*@
lemma void ints_append_nil_lemma(ints values)
    requires true;
    ensures ints_append(values, ints_nil) == values;
{
    switch (values) {
        case ints_nil:
        case ints_cons(h, t):
            ints_append_nil_lemma(t);
    }
}

lemma void ints_append_assoc_lemma(ints values1, ints values2, ints values3)
    requires true;
    ensures ints_append(ints_append(values1, values2), values3) == ints_append(values1, ints_append(values2, values3));
{
    switch (values1) {
        case ints_nil:
        case ints_cons(h, t):
            ints_append_assoc_lemma(t, values2, values3);
    }
}

lemma void ints_reverse_append_lemma(ints values1, ints values2)
    requires true;
    ensures ints_reverse(ints_append(values1, values2)) == ints_append(ints_reverse(values2), ints_reverse(values1));
{
    switch (values1) {
        case ints_nil:
            ints_append_nil_lemma(ints_reverse(values2));
        case ints_cons(h, t):
            ints_reverse_append_lemma(t, values2);
            ints_append_assoc_lemma(ints_reverse(values2), ints_reverse(t), ints_cons(h, ints_nil));
    }
}

lemma void ints_reverse_reverse_lemma(ints values)
    requires true;
    ensures ints_reverse(ints_reverse(values)) == values;
{
    switch (values) {
        case ints_nil:
        case ints_cons(h, t):
            ints_reverse_append_lemma(ints_reverse(t), ints_cons(h, ints_nil));
            ints_reverse_reverse_lemma(t);
    }
}
@*/

void nodes_reverse(list_node *l)
{
    list_node n = *l;
    list_node m = 0;
    while (n != 0)
    {
        list_node next = n->next;
        n->next = m;
        m = n;
        n = next;
    }
    *l = m;
}

